<?php
namespace Drupal\akp_pdf_browser\Controller;

use Drupal\Core\Controller\ControllerBase;

/**
 * Provides route responses for the akp_blocks module.
 */
class PageController extends ControllerBase {

  public function pdf_browser() {
    $element = array(
	  '#theme' => 'pdf_browser',
    );
    return $element;
  }
}