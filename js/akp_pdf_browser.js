/*!
  Javascript - AKP PDF Browser
*/

jQuery(document).ready(function($) {
  Drupal.behaviors.akp_pdf_browser_progressbar = {
      attach: function(context, settings) {
     
          $(document).ajaxComplete(function (event, xhr, settings) {
              $('.pdf-progress-bar').hide();
          });   

      }
  };
  Drupal.Ajax.prototype.beforeSubmit = function (form_values, element, options) {
      $('.pdf-progress-bar').show();
  };

  Drupal.theme.ajaxProgressThrobber = function () {
      $('.pdf-progress-bar').show();
      return;
  };

  var currentWindowHeight = $().getWindowHeight(); //this is needed every time the map is resized (on window resize too)
	currentWindowHeight = currentWindowHeight - 60; //remove 60 temporarily due to differences in the navbar style.
  $("div#main, div#menu-container, div#pdf-container").css('height', currentWindowHeight+'px');

  // Drupal.behaviors.addPdfBrowserInteraction = {
	// 	attach: function (context, settings) {
  //     $(context).find("#menu-container").once("add-interaction").each(function () {
  //       console.log("test");
  //       addPdfBrowserInteraction();
  //     });
	// 	}
	// };
  $('.pdf-progress-bar').hide();

  $('.btn-expand-menu').bind("click", function(){
    $("#menu-container").toggleClass('collapse-column');
    $(this).toggleClass('collapseActivated');
    $("#menu-container button").toggleClass('text-wrap');
    $("#menu-container button").toggleClass('overflow-hidden');
    $(this).tooltip('hide');
  });

  $('button.btn.btn-primary').on( 'click', function() {
      //var nodeID = $(this).closest("div.menu-indicator-card").find(".indicator-nid").text().trim();
      //currentIndicatorNodeURL = '/indicator_card/'+nodeID;
      //console.log(currentIndicatorNodeURL)
      var indicatorURL = $(this).attr("href");
      console.log(indicatorURL);

      document.getElementById("pdf-document-section").innerHTML = "";

      var ajaxSettings = {
        url: indicatorURL,
        wrapper: 'pdf-document-section',
        method: 'prepend',
      };
      var myAjaxObject = Drupal.ajax(ajaxSettings);
      myAjaxObject.execute();
    
  });

});